package tech.mastertech.itau.cassproj.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.cassproj.dtos.LoginResposta;
import tech.mastertech.itau.cassproj.models.Login;
import tech.mastertech.itau.cassproj.services.UsuarioService;

@RestController
@RequestMapping("/login")
public class LoginController {
	
	@Autowired
	private UsuarioService usuarioService;

	@PostMapping
	public LoginResposta fazerLogin(@RequestBody Login credenciais) {
		Optional<String> tokenOptional = usuarioService.fazerLogin(credenciais);

		if (tokenOptional.isPresent()) {
			LoginResposta loginResposta = new LoginResposta();
			loginResposta.setToken(tokenOptional.get());

			return loginResposta;
		}

		throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
	}

}
